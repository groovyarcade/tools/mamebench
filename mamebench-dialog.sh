#!/bin/bash

# --- Variables ---------------------------------------------------------------------------------------------
_temp="/tmp/answer.$$"
TMP="$(mktemp)"

CPU_NAME=""
CPU_MAX_CLOCK=""
CPU_CORES=""
GPU_NAME=""

MAME_PATH=/usr/local/bin/groovymame
ROMS_PATH=/home/arcade/shared/roms/mame/mamebench
LOG_PATH=~/shared/logs/mamebench

BENCH_TIME=30
CSV=roms.csv

DIALOG_HEIGHT=13
DIALOG_WIDTH=80

ROMS_SRC_TYPE=("NOT-CONFIGURED" "USB-DISK" "CIFS-SHARE" "LOCAL-DISK")
ROMS_SRC_SELECTION=0
ROMS_SRC_PATH="NOT-CONFIGURED"

TESTSET=()
TESTSET_FILES=()
TESTSET_STRING=""

CIFS_SERVER=
CIFS_SHARE=
CIFS_USER=
CIFS_PASS=
CIFS_MNT=/tmp/CIFS

NO_CSV=0
DEFAULT=0

# --- General functions ---------------------------------------------------------------------------------------------
getCPUandGPU() {
    for p in /sys/class/drm/card?; do
        id=$(basename $(readlink -f $p/device))
        GPU_NAME=$(lspci -mms $id | cut -d '"' -f4,6 --output-delimiter=" ")
    done

    CPU_NAME=$(lscpu | grep 'Model name' | cut -f 2 -d ":" | awk '{$1=$1}1')
    CPU_MAX_CLOCK=$(lscpu | grep 'CPU max MHz' | cut -f 2 -d ":" |  cut -f 1 -d "." | awk '{$1=$1}1')
    CPU_CORES=$(lscpu | grep 'Core(s) per socket' | cut -f 2 -d ":" | awk '{$1=$1}1')

    return 0
}

readRomsFromCSVDialog() {
    #loop over roms.csv file
    if [ ! -f "$CSV" ]; then
        dialog --pause "Reading ROMS from CSV file rom.csv has failed. \n\nExit programm." $DIALOG_HEIGHT $DIALOG_WIDTH 2
        return 1
    fi

    while IFS="" read -r i || [ -n "$i" ]; do
        #read each line in file into LINE_ARRAY
        IFS=', ' read -r -a LINE_ARRAY <<<"$i"

        #ignore header of line element[0]
        if [[ "${LINE_ARRAY[0]}" != *"rom"* ]]; then
            #append element[0] to array TESTSET
            TESTSET=(${TESTSET[@]} "${LINE_ARRAY[0]}")
        fi

        #loop over elements[1-4]
        for i in {1..4}; do
            ELEMENT="${LINE_ARRAY[$i]}"
            if [[ $ELEMENT != *"chdfolder"* ]] && [[ $ELEMENT != *"file"* ]] && [[ $ELEMENT != *"-"* ]]; then
                #append element[i] to array TESTSET_FILES
                TESTSET_FILES=(${TESTSET_FILES[@]} $ELEMENT)
            fi
        done
    done <"$CSV"

    TESTSET_STRING=$(IFS=$'\n' echo "${TESTSET[*]}")

    dialog --pause "${#TESTSET[@]} roms found in rom.csv" $DIALOG_HEIGHT $DIALOG_WIDTH 2
    return 0
}

# --- Fuctions USB ---------------------------------------------------------------------------------------------
checkUSBdiskmountDialog() {
    if ! $(grep -qs '/run/media/arcade/' /proc/mounts); then
        dialog --pause "No USB disk auto mountpoint found" $DIALOG_HEIGHT $DIALOG_WIDTH 2
        ROMS_SRC_PATH="NOT-CONFIGURED"
        ROMS_SRC_SELECTION=0

        return 1
    fi

    ROMS_SRC_PATH=$(mount | grep /run/media/arcade/ | awk -F " " '{print $3}')
    ROMS_SRC_PATH=$(find $ROMS_SRC_PATH -type f -name "pong.zip" | sed -r 's/pong.zip//')
    ROMS_SRC_SELECTION=1

    dialog --pause "USB disk auto mount point and pong.zip found\nSet ROMS_SRC_PATH to: $ROMS_SRC_PATH" $DIALOG_HEIGHT $DIALOG_WIDTH 2
    return 0
}

# --- Fuctions CIFS mount ---------------------------------------------------------------------------------------------
checkPackages() {
    for PACKAGE in "$@"; do
        if ! pacman -Qs $PACKAGE &>/dev/null; then
            echo -e "- The package $PACKAGE is not installed. Installing..."
            sudo pacman -S $PACKAGE
        fi
        echo -e "- The package $PACKAGE is installed"
    done
    return 0
}

checkKernelModule() {
    MODULE=$1
    if ! lsmod | grep $1 &>/dev/null; then
        echo -e "- The kernel module $1 is not loaded. Loading...\n"
        sudo modprobe $1
        return 0
    fi
    echo -e "- The kernel module $1 is loaded\n"
    return 0
}

checkCifsServer() {
    nc -z -n -w 2 -v $1 139 >/dev/null 2>&1 || return 1
    nc -z -n -v $1 445 >/dev/null 2>&1 || return 1
    return 0
}

mountCifsShareDialog() {
    clear

    if mountpoint -q $CIFS_MNT; then
        MOUNT=$(mount | grep $CIFS_MNT | awk '{print $1,$2,$3}')
        ROMS_SRC_PATH=$(cat "$TMP")
        ROMS_SRC_SELECTION=2
        dialog --pause "Mount already present: $MOUNT\nROMS_SRC_PATH set to $ROMS_SRC_PATH" $DIALOG_HEIGHT $DIALOG_WIDTH 2
        return 1
    fi

    echo -e "Checing if needed packages are already installed"
    checkPackages cifs-utils gnu-netcat smbclient ranger

    echo -e "Checking if needed kernel module is loaded"
    checkKernelModule cifs

    echo -e "Continue to mount parameters..."
    sleep 2

    SERVER="$CIFS_SERVER"
    SHARE="$CIFS_SHARE"
    USER="$CIFS_USER"
    mkdir -p $CIFS_MNT

    output="/tmp/out.tmp"
    trap "rm -f /tmp/out.tmp" 2 15

    dialog --title "CIFS mount configuration" \
        --inputbox "Please enter your CIFS server ip or hostname" 10 30 2>$output
    SERVER=$(cat /tmp/out.tmp)

    dialog --title "CIFS mount configuration" \
        --inputbox "Please enter your CIFS user account" 10 30 2>$output
    USER=$(cat /tmp/out.tmp)

    dialog --title "Password" \
        --insecure \
        --clear \
        --passwordbox "Please enter password" 10 30 2>$output
    CIFS_PASS=$(cat /tmp/out.tmp)

    rm -f /tmp/out.tmp

    SHARE="/$(smbclient -L \\\\$SERVER -U $USER%$CIFS_PASS | grep Disk | awk '{print $1}')"
    clear

    if ! checkCifsServer $SERVER; then
        dialog --pause "The CIFS Server $SERVER TCP ports can not be reached after 2 seconds" $DIALOG_HEIGHT $DIALOG_WIDTH 2
    fi

    if [[ $USER == guest ]]; then
        # Guest only works on SMB 1.0 and 2.0
        sudo mount -t cifs //$SERVER$SHARE $CIFS_MNT -o guest,vers=2.0
    else
        if [[ -z $CIFS_PASS ]]; then
            sudo mount -t cifs //$SERVER$SHARE $CIFS_MNT -o username=$USER,vers=2.0
        else
            sudo PASSWD="$CIFS_PASS" mount -t cifs //$SERVER$SHARE $CIFS_MNT -o username=$USER,vers=2.0
        fi
    fi
    if ! mountpoint $CIFS_MNT >/dev/null 2>&1; then
        dialog --pause "Mounting CIFS share failed" $DIALOG_HEIGHT $DIALOG_WIDTH 2
        ROMS_SRC_PATH="NOT-CONFIGURED"
        ROMS_SRC_SELECTION=0
        return 1
    fi

    MOUNT=$(mount | grep $SERVER | awk '{print $1,$2,$3}')

    dialog --pause "Please use ranger file browser to select the roms directory\nThen press 'q' to quit" $DIALOG_HEIGHT $DIALOG_WIDTH 2

    clear

    ranger $CIFS_MNT --show-only-dir --choosedir="$TMP"
    ROMS_SRC_PATH=$(cat "$TMP")
    ROMS_SRC_SELECTION=2

    dialog --pause "Mount present: $MOUNT\nSet ROMS_SRC_PATH to $ROMS_SRC_PATH" $DIALOG_HEIGHT $DIALOG_WIDTH 2

    return 0
}

umountCifsShareDialog() {
    sudo umount "$CIFS_MNT"

    ROMS_SRC_PATH="NOT-CONFIGURED"
    ROMS_SRC_SELECTION=0

    dialog --pause "Unmouted $CIFS_MNT\nSet ROMS_SRC_PATH to $ROMS_SRC_PATH" $DIALOG_HEIGHT $DIALOG_WIDTH 2
}

# --- Fuctions LOCAL DISK ---------------------------------------------------------------------------------------------
copyRomsDialog() {

    if [[ ! -z "$(ls -A $ROMS_PATH)" ]]; then
        ROMS_SRC_PATH=$ROMS_PATH
        ROMS_SRC_SELECTION=3
        dialog --pause "Local directory is not empty\nNo need to copy roms\nSetting ROMS_SRC_PATH: $ROMS_PATH " $DIALOG_HEIGHT $DIALOG_WIDTH 2
        return 1
    fi

    if [[ $ROMS_SRC_PATH = "NOT-CONFIGURED" ]]; then
        dialog --pause "ROMS_SRC_PATH not set\nCannot copy roms\nPlease set up USB mount or CIFS share first" $DIALOG_HEIGHT $DIALOG_WIDTH 2
        return 1
    fi

    dialog --pause "Start copying files from $ROMS_SRC_PATH to $ROMS_PATH" $DIALOG_HEIGHT $DIALOG_WIDTH 2

    clear
    mkdir -p "$ROMS_PATH"

    for i in "${TESTSET_FILES[@]}"; do
        rsync -rvh --progress --ignore-existing "$ROMS_SRC_PATH"/$i "$ROMS_PATH"
    done

    FILES_TO_COPY="${#TESTSET_FILES[@]}"
    FILES_COPIED=$(ls -l $ROMS_PATH | wc -l)
    ((FILES_COPIED--)) #substract the initial output string ls

    dialog --title "Copy ROMS" --pause "$FILES_TO_COPY files to copy. \n$FILES_COPIED found in $ROMS_PATH. \nSet ROMS_SRC_PATH to $ROMS_PATH." $DIALOG_HEIGHT $DIALOG_WIDTH 2

    ROMS_SRC_PATH=$ROMS_PATH
    ROMS_SRC_SELECTION=3
    return 0 #TODO: Implement failure return value of this function
}

deleteBenchroms() {
    clear
    rm -rf $ROMS_PATH/*
    ROMS_SRC_PATH="NOT-CONFIGURED"
    ROMS_SRC_SELECTION=0

    dialog --title "Delete local bench roms source" --pause "Deleted all files in $ROMS_PATH" $DIALOG_HEIGHT $DIALOG_WIDTH 2
    return 0
}

# --- Fuctions benchmark ---------------------------------------------------------------------------------------------
runBenchmarks() {
    clear

    killall attract
    killall attractplus

    if [ "$ROMS_SRC_PATH" == "NOT-CONFIGURED" ]; then
        dialog --pause "Error: ROMS_SRC_PATH not set" $DIALOG_HEIGHT $DIALOG_WIDTH 2
        return 1
    fi

    echo -e "ROMS_SRC_PATH: $ROMS_SRC_PATH"
    echo -e "LOG_PATH: $LOG_PATH\n\n"
    echo -e "### RUN BENCHMARKS"

    TIMESTAMP=$(date "+%Y-%m-%d")
    [[ ! -d "$LOG_PATH" ]] && mkdir -p "$LOG_PATH"

    LOGFILE="$LOG_PATH\benchmark_$TIMESTAMP.log"
    [[ ! -f "$LOGFILE" ]] && touch "$LOGFILE"

    GROOVYMAME="$MAME_PATH"
    [[ $DEFAULT != 1 ]] && PARAMETERS_BENCH="-rompath $ROMS_SRC_PATH"
    PARAMETERS_BENCH="$PARAMETERS_BENCH -bench $BENCH_TIME -nothrottle -nosleep -noautoframeskip -skip_gameinfo -effect none"

    for GAME in "${TESTSET[@]}"; do
        echo -e "Running ${BENCH_TIME}s benchmark for: $GAME"
        ROMDESCRIPTION=$($GROOVYMAME -listfull $GAME | awk 'NR==2' | awk -F'"' '{print $2}')
        echo "Description: $ROMDESCRIPTION" >>$LOGFILE
        $GROOVYMAME $PARAMETERS_BENCH $GAME |& tee -a $LOGFILE |& tee "$LOG_PATH"/"$GAME".log &>/dev/null
    done
    return 0
}

buildStats() {
    stats_data=""
    if [[ $NO_CSV == 1 ]]; then
        ROMLIST=$(ls ~/shared/logs/mamebench/*.log | xargs basename -s .log)
    else
        ROMLIST="${TESTSET[@]}"
    fi
    # Loop on roms
    for GAME in $ROMLIST; do
        [[ ! -e "$LOG_PATH"/"$GAME".log ]] && continue
        fd_data=$(grep "^Frame delay/percentage" "$LOG_PATH"/"$GAME".log) || continue
        # Loop on FD in [0,9]
        line="$GAME"
        echo -n "Scanning $GAME..."
        for i in {0..9}; do
            # Extract the percentage if it exists
            val=$(echo $fd_data | egrep -o "$i/[0-9.\%]+" | cut -d '/' -f2)
            line="$line|$val"
        done
        avg_pct=$(grep "^Average speed" "$LOG_PATH"/"$GAME".log | egrep -o "[0-9.\%]+" | head -1)
        echo -en "\r" >&2
        stats_data="$(echo -e "$stats_data\n$line|$avg_pct")"
    done

    echo -e "Run of mamemench.sh on $(date '+%Y-%m-%d')\n" >$LOG_PATH/stats.log
    echo -e "CPU: $CPU_NAME" >>$LOG_PATH/stats.log
    echo -e "CPU_MAX_CLOCK: $CPU_MAX_CLOCK MHz" >>$LOG_PATH/stats.log
    echo -e "CPU_CORES: $CPU_CORES\n" >>$LOG_PATH/stats.log
    echo -e "GPU: $GPU_NAME\n" >>$LOG_PATH/stats.log



    echo -e "$stats_data" | column -t -s '|' -o " | " -N "ROM \ FrameDelay,0,1,2,3,4,5,6,7,8,9,Avg speed" -R "2,3,4,5,6,7,8,9,10,11,12" >>"$LOG_PATH"/"stats.log"
    return 0
}

showStatsDialog() {
    clear

    if [[ -z "$(ls -A $LOG_PATH)" ]]; then
        dialog --pause "There are no logs in $LOG_PATH\nPlease run benchmark first" $DIALOG_HEIGHT $DIALOG_WIDTH 2
        return 1
    fi

    echo -e "Building stats from the logs of last run"
    buildStats

    if [[ ! -f "$LOG_PATH"/"stats.log" ]]; then
        dialog --pause "There was an error\n$LOG_PATH"/"stats.log does not exist" $DIALOG_HEIGHT $DIALOG_WIDTH 2
        return 1
    fi

    dialog --pause "Please access stats.log via ssh\n$LOG_PATH/stats.log" $DIALOG_HEIGHT $DIALOG_WIDTH 10
    return 0
}

eraseBenchLogs() {
    clear
    rm -rf $LOG_PATH/*
    dialog --title "Delete local bench logs" --pause "Deleted all files in $LOG_PATH" $DIALOG_HEIGHT $DIALOG_WIDTH 2
    return 0
}

# --- Fuctions main ---------------------------------------------------------------------------------------------
mainMenuDialog() {

    CURRENT_CONFIG="$CPU_NAME
    \n$CPU_MAX_CLOCK MHz, $CPU_CORES Cores
    \n$GPU_NAME
    \n\nROMS_SRC_TYPE: ${ROMS_SRC_TYPE[$ROMS_SRC_SELECTION]}
    \nROMS_SRC_PATH: $ROMS_SRC_PATH
    \nBENCH_TIME: $BENCH_TIME sec"

    dialog --backtitle "Mamebench - benchmark tool GroovyArcade community" \
        --title "Mamebench current config " \
        --cancel-label "Quit" \
        --menu "$CURRENT_CONFIG" $DIALOG_HEIGHT $DIALOG_WIDTH 0 \
        UsbDisk "Setup USB-DISK disk as roms source" \
        CifsShare "Setup CIFS-SHARE disk as roms source" \
        UnmountCifs "Unmount CIFS-SHARE if present" \
        LocalDisk "Setup LOCAL-DISK as roms source " \
        DeleteLocal "Delete local bench roms source" \
        Run "Start mamebench" \
        Stats "Show statistics from last benchmark run" \
        Erase "Erase local logfiles" 2>$_temp

    opt=${?}
    if [ $opt != 0 ]; then
        rm $_temp
        exit
    fi

    menuitem=$(cat $_temp)
    echo "menu=$menuitem"

    case $menuitem in
    UsbDisk) checkUSBdiskmountDialog ;;
    CifsShare) mountCifsShareDialog ;;
    UnmountCifs) umountCifsShareDialog ;;
    LocalDisk) copyRomsDialog ;;
    DeleteLocal) deleteBenchroms ;;
    Run) runBenchmarks ;;
    Stats) showStatsDialog ;;
    Erase) eraseBenchLogs ;;
    esac
}

# --- MAIN PROGRAMM -------------------------------------------------------------
if ! readRomsFromCSVDialog; then
    exit 1
fi

getCPUandGPU

while true; do
    mainMenuDialog
done

umountCifsShareDialog

exit 0
