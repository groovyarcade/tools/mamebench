#!/bin/bash

# $1: alternative json file to download
uri_file=uris.txt
# Define required packages, including aria2c
#required_packages=("jq" "aria2")

# Install required packages if not already installed
for package in "${required_packages[@]}"; do
    pacman -Q "$package" &> /dev/null || sudo pacman -S "$package"
done

# Function to display a formatted message in yellow
show_message() {
    local title="$1"
    local message="$2"
    echo -e "\n\e[1;33m[${title}] $message\e[0m"
    #sleep 2
}


[[ -e "$uri_file" ]] && rm "$uri_file"
[[ -e webpage.html ]] && rm webpage.html

# JSON file path
json_file="roms.json"
[[ -n "$1" ]] && json_file="$1"
echo $json_file

# Check if the JSON file exists
[[ -f "$json_file" ]] || { show_message "Error" "JSON file not found: $json_file"; exit 1; }

# Show "JSON File Found" message
show_message "INFO" "Checking '$json_file' for needed files"

# Get the list of files needed from the JSON data and sort alphabetically
needed_files="$(jq -r '.[].files[]' "$json_file" | sort | uniq)"
needed_chds="$(jq -r '.[] | [.rom, ":", (.chdfiles | join(","))?] | join("")' "$json_file")"

# Count the number of files
zips_count=$(echo "$needed_files" | wc -l)
chds_count=$(echo "$needed_chds" | wc -l)

# Display the number of files and the list of files on a single line with spaces
formatted_files=$(echo "$needed_files" | tr '\n' ' ')
formatted_chds=$(echo "$needed_chds" | tr '\n' ' ')
show_message "INFO" "$zips_count zips needed: $formatted_files"
show_message "INFO" "$chds_count CHDs needed: $formatted_chds"

# URL to the web page containing the links
url_files="https://archive.org/download/mame-merged/mame-merged/"
url_chds="https://archive.org/download/MAME_0.225_CHDs_merged/"

# Display a message while downloading the webpage using aria2c
show_message "INFO" "Downloading website $url_files for link analyses"
aria2c "$url_files" -o "webpage_files.html" -x 10

# Create the directory if it doesn't exist
mkdir -p ~/shared/roms/mame/mamebench/

# Iterate over the needed files and check if they exist in the directory
for file in $needed_files; do
    if [[ ! -f ~/shared/roms/mame/mamebench/"$file" ]]; then
        # If the file is missing, find the full URL in the downloaded webpage
        full_url=$(grep -o "href=\"$file\">" webpage_files.html | sed -n 's/.*href="\([^"]*\)">.*/\1/p')
        if [[ -n "$full_url" ]]; then
            # Display the "Downloading" message
            show_message "INFO" "Appending $url$file"
            echo "$url_files$file" >> "$uri_file"
            # Download the file to the directory using aria2c
            #aria2c -x10 "$url$file" -d "$HOME/shared/roms/mame/mamebench" -o "$file"
        fi
    else
        show_message "INFO" "$file already on filesystem"
    fi
done
[[ -e "$uri_file" ]] && aria2c -i "$uri_file" -d "$HOME/shared/roms/mame/mamebench" -x 16 -k 1M -s 10

# CHD time now
for file in $needed_chds; do
    romName="$(echo $file | cut -d ':' -f 1)"
    chdsList="$(echo $file | cut -d ':' -f 2)"
    [[ -z $chdsList ]] && continue
    [[ ! -d ~/shared/roms/mame/mamebench/"$romName" ]] && mkdir -p ~/shared/roms/mame/mamebench/"$romName"/
    # If the file is missing, find the full URL in the downloaded webpage
    show_message "INFO" "Downloading website $url_chds$romName/ for link analyses"
    aria2c "$url_chds$romName/" -o "webpage_chds.html" -x 10
    for chdFile in $(echo $chdsList | tr ',' ' ') ; do
        if [[ -e "$HOME/shared/roms/mame/mamebench/$romName/$chdFile" ]] ; then
            show_message "INFO" "$chdFile already on filesystem"
            continue
        fi
        full_url=$(grep -o "href=\"$chdFile\">" webpage_chds.html | sed -n 's/.*href="\([^"]*\)">.*/\1/p')
        if [[ -n "$full_url" ]]; then
            # Download the file to the directory using aria2c
            aria2c -x 16 -k 1M -s 20 "$url_chds$romName/$chdFile" -d "$HOME/shared/roms/mame/mamebench/$romName/" -o "$chdFile"
        else
            show_message "ERROR" "Couldn't download $chdFile"
        fi
    done
    rm "webpage_chds.html" 2>/dev/null
done


# Clean up the downloaded HTML file
rm -f webpage_files.html webpage_chds.html #"$uri_file"

# End of the script
