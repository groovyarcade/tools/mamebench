#!/bin/bash

# --- MODULES ------------------
DIR=""
TESTSET=()
TESTSET_FILES=()
BENCH_TIME=30
CSV=roms.csv
DONT_RUN_BENCH=0
DONT_MOUNT_CIFS=0
DONT_COPY_ROMS=0
CIFS_SERVER=
CIFS_SHARE=
CIFS_USER=
CIFS_PASS=
CIFS_MNT=/tmp/CIFS
NO_CSV=0
MAME_PATH=/usr/local/bin/groovymame
ROMS_PATH=/tmp/mamebenchroms
DEFAULT=0
GAMELOG=~/shared/logs/mamebench

usage() {
    echo "mamebench is a small tool for benchmarking GroovyMAME

Options:
	-h, --help		Help
	-t, --time <seconds>	How many seconds should mame run for the bench
	-c, --csv <file>	Specify a different csv file used for the roms to bench
	-u, --user <user>	Set the CIFS user rather than interactively
	-p, --password <pass>	Set the CIFS password rather than interactively (not recommended)
	-m, --mame <file>	Use a different GroovyMAME binary (default: $MAME_PATH)
	-r, --rom-path <path>	Specify a different rompath (default: $ROMS_PATH)
	-d, --default		Use the default GroovyMAME rom path. Also forces the following options:
				--no-cifs, --no-copy
	--stats-only		Don't bench, just generate stats from the last run logs
	--no-cifs		Don't mount a CIFS share
	--no-copy		Don't copy the roms from the CIFS share to the roms path (see -r)
	--cifs-server <ip>	Specify the CIFS server to connect to. Can be a hostname
	--cifs-share <path>	The full path to the CIFS roms folder (ex: /roms/mame)
	--clean			Remove the bench logs from previous runs
	--no-csv		When building stats, don't stick to the roms listed in the CSV, but take all roms from the bencj logs dir

CSV file format:
mamebench gets the list of roms to bench from a csv file. The format is:
rom;chdfolder;file1;file2;file3...
	rom: the rom name without the .zip extension
	chdfolder: the name of the folder for CHDs
	fileN: additionnal files (like bios, parent rom of a clone, extra required files etc ...)
The first line is considered as a header line, so no copies will happen with it.

Tips:
	- When using --stats-only, the list of roms will be taken from the CSV unless you add --no-csv
	  With --no-csv stats will consider all bench log files available
	- Unless you use --clean, logs from previous runs are kept. Combined with -c and --stats-only,
	  you can bench groups of roms one after another with -c and in the end, simply get the full stats with
	  -d --stats-only --no-csv
"
}

checkPackages() {
    for PACKAGE in "$@"; do
        if ! pacman -Qs $PACKAGE &>/dev/null; then
            echo -e "- The package $PACKAGE is not installed. Installing..."
            sudo pacman -S $PACKAGE
        fi
        echo -e "- The package $PACKAGE is installed"
    done
    return 0
}

checkKernelModule() {
    MODULE=$1
    if ! lsmod | grep $1 &>/dev/null; then
        echo -e "- The kernel module $1 is not loaded. Loading..."
        sudo modprobe $1
        return 0
    fi
    echo -e "- The kernel module $1 is loaded"
    return 0
}

checkCifsServer() {
    nc -z -n -w 2 -v $1 139 >/dev/null 2>&1 || return 1
    nc -z -n -v $1 445 >/dev/null 2>&1 || return 1
    return 0
}

mountCifsShare() {
    echo -e "\n"
    echo -e "### MOUNT OF CIFS SHARE WITH MAME ROMS "

    echo -e "- Checking if needed packages are already installed"
    checkPackages cifs-utils gnu-netcat

    echo -e "- Checking if needed kernel module is loaded"
    checkKernelModule cifs
    DIR="$CIFS_MNT"
    if mountpoint -q $DIR ; then
        MOUNT=$(mount | grep $DIR | awk '{print $1,$2,$3}')
        echo -e "- Mount already present: $MOUNT"
        return 0
    fi

    SERVER="$CIFS_SERVER"
    [[ -z "$SERVER" ]] && read -p '- Input CIFS Server IP or HOSTNAME: ' SERVER
    if ! checkCifsServer $SERVER; then
        echo -e "- The CIFS Server TCP Ports can not be reached after 2 seconds"
        return 1
    fi
    echo -e "- The CIFS Server TCP Ports can be reached"

    SHARE="$CIFS_SHARE"
    [[ -z "$SHARE" ]] && read -p '- Input CIFS Share, e.g /MAMEROMS: ' SHARE
    USER="$CIFS_USER"
    [[ -z $USER ]] && read -p '- Input CIFS Server Username: ' USER
    mkdir -p $DIR

    if [[ $USER == guest ]] ; then
        # Guest only works on SMB 1.0 and 2.0
        sudo mount -t cifs //$SERVER$SHARE $DIR -o guest,vers=2.0
    else
        if [[ -z $CIFS_PASS ]] ; then
            sudo mount -t cifs //$SERVER$SHARE $DIR -o username=$USER,vers=2.0
        else
            sudo PASSWD="$CIFS_PASS" mount -t cifs //$SERVER$SHARE $DIR -o username=$USER,vers=2.0
        fi
    fi
    if ! mountpoint $DIR >/dev/null 2>&1; then
        echo -e "- Mounting failed"
        return 1
    fi

    MOUNT=$(mount | grep $SERVER | awk '{print $1,$2,$3}')
    echo -e "- Mount present: $MOUNT"
    return 0
}

readRomsFromCSV(){
    #loop over roms.csv file
    while IFS="" read -r i || [ -n "$i" ]
    do
        #read each line in file into LINE_ARRAY
        IFS=', ' read -r -a LINE_ARRAY <<< "$i"

        #ignore header of line element[0]
        if [[ "${LINE_ARRAY[0]}" != *"rom"* ]]; then
            #append element[0] to array TESTSET
            TESTSET=(${TESTSET[@]} "${LINE_ARRAY[0]}")
        fi

        #loop over elements[1-4]
        for i in {1..4}; do
            ELEMENT="${LINE_ARRAY[$i]}"
            if [[ $ELEMENT != *"chdfolder"* ]] && [[ $ELEMENT != *"file"* ]] && [[ $ELEMENT != *"-"* ]]; then
                #append element[i] to array TESTSET_FILES
                TESTSET_FILES=(${TESTSET_FILES[@]} $ELEMENT)
            fi
        done
    done < "$CSV"
    return 0
}

copyRoms() {
    echo -e "\n"
    echo -e "### COPY MAME ROMS TO LOCAL DISK"

    mkdir -p "$ROMS_PATH"

    for i in "${TESTSET_FILES[@]}"; do
        rsync -rvh --progress --ignore-existing "$CIFS_MNT"/$i "$ROMS_PATH"
        echo -e "\n"
    done

    FILES_TO_COPY="${#TESTSET_FILES[@]}"
    FILES_COPIED=$(ls -l ~/shared/roms/mame/testset/ | wc -l)
    ((FILES_COPIED--)) #substract the initial output string ls

    echo -e "- $FILES_TO_COPY files to copy, $FILES_COPIED found in $ROMS_PATH "
    return 0 #TODO: Implement failure return value of this function
}

runBenchmarks() {
    echo -e "\n### RUN BENCHMARKS"

    TIMESTAMP=$(date "+%Y-%m-%d")
    mkdir -p ~/shared/logs/
    LOGFILE=~/shared/logs/benchmark_$TIMESTAMP.log
    touch $LOGFILE

    GROOVYMAME="$MAME_PATH"
    [[ $DEFAULT != 1 ]] && PARAMETERS_BENCH="-rompath $ROMS_PATH"
    PARAMETERS_BENCH="$PARAMETERS_BENCH -bench $BENCH_TIME -nothrottle -nosleep -noautoframeskip -skip_gameinfo -effect none"

    [[ ! -d "$GAMELOG" ]] && mkdir -p "$GAMELOG"

    for GAME in "${TESTSET[@]}"; do
        echo -e "Running ${BENCH_TIME}s benchmark for: $GAME"
        ROMDESCRIPTION=$($GROOVYMAME -listfull $GAME | awk 'NR==2' | awk -F'"' '{print $2}')
        echo "Description: $ROMDESCRIPTION" >>$LOGFILE
        $GROOVYMAME $PARAMETERS_BENCH $GAME |& tee -a $LOGFILE |& tee "$GAMELOG"/"$GAME".log &>/dev/null
        echo -e "\n" >>$LOGFILE
    done

    return 0
}

buildStats() {
    stats_data=""
    if [[ $NO_CSV == 1 ]] ; then
        ROMLIST=$(ls ~/shared/logs/mamebench/*.log | xargs basename -s .log)
    else
        ROMLIST="${TESTSET[@]}"
    fi

    # Loop on roms
    for GAME in $ROMLIST; do
        [[ ! -e "$GAMELOG"/"$GAME".log ]] && continue
        fd_data=$(grep "^Frame delay/percentage" "$GAMELOG"/"$GAME".log) || continue
        # Loop on FD in [0,9]
        line="$GAME"
        echo -n "Scanning $GAME..."
        for i in {0..9} ; do
            # Extract the percentage if it exists
            val=$(echo $fd_data | egrep -o "$i/[0-9.\%]+" | cut -d '/' -f2)
            line="$line|$val"
        done
        avg_pct=$(grep "^Average speed" "$GAMELOG"/"$GAME".log | egrep -o "[0-9.\%]+" | head -1)
        echo -en "\r" >&2
        stats_data="$(echo -e "$stats_data\n$line|$avg_pct")"
    done
    echo "$stats_data" | column -t -s '|' -o " | " -N "ROM \ FrameDelay,0,1,2,3,4,5,6,7,8,9,Avg speed" -R "2,3,4,5,6,7,8,9,10,11,12"
}


# --- MAIN PROGRAMM ------------------

TEMP=$(getopt -o hc:u:p:t:m:r:d -l help,csv,user,password,time,mame,rom-path:,stats-only,no-cifs,no-copy,cifs-server:,cifs-share:,default,clean,no-csv -- "$@")

eval set -- "$TEMP"
while true; do
  # Useful to debug options parsing
  #echo $*
  case "$1" in
    -h | --help )	usage ; exit 0 ;;
    -t | --time )	BENCH_TIME="$2"; shift 2 ;;
    -c | --csv )	CSV="$2" ; shift 2 ;;
    -u | --user )	CIFS_USER="$2" ; shift 2 ;;
    -p | --password )	CIFS_PASS="$2" ; shift 2 ;;
    -m | --mame )	MAME_PATH="$2" ; shift 2 ;;
    -r | --rom-path )	ROMS_PATH="$2" ; shift 2 ;;
    -d | --default )	DEFAULT=1 ; DONT_MOUNT_CIFS=1 ; DONT_COPY_ROMS=1 ; shift ;;
    --stats-only )	DONT_RUN_BENCH=1 ; shift ;;
    --no-cifs )		DONT_MOUNT_CIFS=1 ; shift ;;
    --no-copy )		DONT_COPY_ROMS=1 ; shift ;;
    --cifs-server )	CIFS_SERVER="$2" ; shift 2 ;;
    --cifs-share )	CIFS_SHARE="$2" ; shift 2 ;;
    --clean )		rm -rf "$GAMELOG" ; shift ;;
    --no-csv )		NO_CSV=1 ; shift ;;
    -- ) shift ; break ;;
    * ) break ;;
  esac
done
#exit 0

if [[ $DONT_MOUNT_CIFS != 1 ]] ; then
    if ! mountCifsShare; then
        echo -e "-Mounting CIFS share has failed"
        exit 1
    fi
fi

if ! readRomsFromCSV ; then
    echo -e "-Reading in the ROMS from CSV file has failed"
fi

if [[ $DONT_COPY_ROMS != 1 ]] ; then
    if ! copyRoms; then
        echo -e "- Error: Not all ROMS copied"
        exit 2
    fi
fi

if [[ $DONT_RUN_BENCH != 1 ]] ; then
    runBenchmarks
fi

echo 'Building the final stats...'
buildStats

if [[ $DONT_MOUNT_CIFS != 1 ]] && mountpoint -q "$CIFS_MNT" ; then
    echo "Unmounting: $CIFS_MNT"
    sudo umount "$CIFS_MNT"
fi

exit 0