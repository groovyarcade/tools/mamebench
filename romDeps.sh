#!/bin/bash
xmlCacheDir=/tmp/mb
fullJson="["

[[ ! -e "$xmlCacheDir" ]] && mkdir -p "$xmlCacheDir"
# Download a XML file from ADB and return it
downloadXMLFromADB() {
	name="$1"
	destFile="$xmlCacheDir"/"${name}".xml
	if [[ ! -e "$destFile" ]] ; then
		wget -qO "$destFile" "http://adb.arcadeitalia.net/download_file.php?tipo=xml&codice=${name}&emulator=&oper=download&rename=${name}.xml"
	fi
	cat "$destFile"
}

#for r in bm3 starblad kof98 kungfum gauntleg sfiii ; do
for r in "$@" ; do
	# Build the deps list
	deps=""
	xmlData="$(downloadXMLFromADB $r)"
	for a in "device_ref/@name" "@romof" "@cloneof" ; do
	echo "$r -> $a"
		deps+=" $(downloadXMLFromADB "$r" | xmllint --xpath "/machine/$a" - 2>/dev/null | cut -d '"' -f 2 | sort | uniq | tr '\n' ' ')"
	done
	echo $deps

	# Take care of CHDs
	# xargs helps to remove the trailling white space
	chd="$(downloadXMLFromADB "$r" | xmllint --xpath "/machine/disk/@name" - 2>/dev/null | cut -d '"' -f 2 | sort | uniq | sed 's/$/.chd/' | tr '\n' ' ' | xargs)"
	# Now deps are done, so check if deps are "roms"
	realDeps="$r.zip"
	for d in $deps ; do
		echo -n "$d "
		depRoms="$(downloadXMLFromADB "$d" | xmllint --xpath "/machine/rom/@name" - 2>/dev/null)"
		if [[ -n $depRoms ]] ; then
			realDeps+=" $d.zip"
		fi
	done
	echo
	# Need to build the roms.json here
	echo "$r -> $realDeps / '$chd'"
	fullJson+="\n$(jq -n -M \
		--arg rom "$r" \
		--arg files "$realDeps" \
		--arg chd "$chd" \
		'{rom: $rom, chdfiles: ($chd|split(" ")), files: ($files|split(" "))}'),"
done
fullJson="${fullJson::-1}\n]"
echo -e "$fullJson"
echo -e "$fullJson" | jq -M . > myroms.json
#rm -rf "$xmlCacheDir"